function findRemoteFunc (list, funcList, tokenFuncList, blankList) {
  for (let i = 0; i < list.length; i++) {
    if (list[i].type == 'grid') {
      list[i].columns.forEach(item => {
        findRemoteFunc(item.list, funcList, tokenFuncList, blankList)
      })
    } else {
      if (list[i].type == 'blank') {
        if (list[i].model) {
          blankList.push({
            name: list[i].model,
            label: list[i].name
          })
        }
      } else if (list[i].type == 'imgupload') {
        if (list[i].options.tokenFunc) {
          tokenFuncList.push({
            func: list[i].options.tokenFunc,
            label: list[i].name,
            model: list[i].model
          })
        }
      } else {
        if (list[i].options.remote && list[i].options.remoteFunc) {
          funcList.push({
            func: list[i].options.remoteFunc,
            label: list[i].name,
            model: list[i].model
          })
        }
      }
    }
  }
}

export default function (data) {

  const funcList = []

  const tokenFuncList = []

  const blankList = []

  const list = JSON.parse(data).list
  const form = JSON.parse(data)
  findRemoteFunc(JSON.parse(data).list, funcList, tokenFuncList, blankList)
  let funcTemplate = ''

  let blankTemplate = ''

  let listTemplate =''

  let tabelTemplate =''

  let options =''
  for(let i = 0; i < list.length; i++) {


    if(list[i].options.options){
      options += list[i].model+'s: ' +JSON.stringify(list[i].options.options) +','

      listTemplate += `
          <el-form-item label=${list[i].name}>
            <el-${list[i].type} v-for="item in ${list[i].model}s" :label="item.value" :key="item.value"></el-${list[i].type}>
          </el-form-item>
    `
    }else {
      listTemplate += `
          <el-form-item label=${list[i].name}>
            <el-${list[i].type}></el-${list[i].type}>
          </el-form-item>
    `
    }
    tabelTemplate +=`
     <el-table-column prop="${list[i].model}" label="${list[i].name}" width="140">
        </el-table-column>
    `
  }

  for(let i = 0; i < funcList.length; i++) {
    funcTemplate += `
            ${funcList[i].func} (resolve) {
              // ${funcList[i].label} ${funcList[i].model}
              // 获取到远端数据后执行回调函数
              // resolve(data)
            },
    `
  }

  for(let i = 0; i < tokenFuncList.length; i++) {
    funcTemplate += `
            ${tokenFuncList[i].func} (resolve) {
              // ${tokenFuncList[i].label} ${tokenFuncList[i].model}
              // 获取到token数据后执行回调函数
              // resolve(token)
            },
    `
  }

  for (let i = 0; i < blankList.length; i++) {
    blankTemplate += `
        <template slot="${blankList[i].name}" slot-scope="scope">
          <!-- ${blankList[i].label} -->
          <!-- 通过 v-model="scope.model.${blankList[i].name}" 绑定数据 -->
        </template>
    `
  }
  debugger

  return `
<template>
  <div>
    <el-header class="bd-row">
      <el-button @click="dialogFormVisible = true">新建</el-button>
    </el-header>

    <el-main class="bd-row">
      <el-table :data="tableData">
        ${tabelTemplate}
      </el-table>
      <el-dialog :visible="dialogFormVisible">
        <el-form :label-position=labelPosition>
          ${listTemplate}
        </el-form>
        <span slot="footer" class="dialog-footer">
            <el-button @click="dialogFormVisible = false">取 消</el-button>
            <el-button type="primary" @click="dialogFormVisible = false">确 定</el-button>
          </span>
      </el-dialog>
    </el-main>
  </div>
</template>

<script>
  export default {
    name: "Demo",
    data() {
      return {
        dialogFormVisible: false,
        labelPosition: '${form.labelPosition}',
        ${options}
      }
    }
  }
</script>

<style scoped>

</style>

`
}
